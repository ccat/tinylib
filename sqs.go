//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	//"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func SendSQSMessage(message map[string]string, queue_name string, delaySeconds int64) error {
	jsonData, err := json.Marshal(message)
	if err != nil {
		return err
	}

	svc := configGlobal.GetSQS()
	params := &sqs.SendMessageInput{
		MessageBody:  aws.String(string(jsonData)),
		QueueUrl:     aws.String(configGlobal.GetSQSBaseURL() + queue_name),
		DelaySeconds: aws.Int64(delaySeconds),
	}

	//sqsRes, err := svc.SendMessage(params)
	_, err = svc.SendMessage(params)
	return err
}
