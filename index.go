package tinylib

import (
	//	"github.com/guregu/dynamo"
	"time"

	"github.com/pkg/errors"
)

type Index struct {
	Prime string `dynamo:"prime"`
	Sort  string `dynamo:"sort"`
	Num   int64
	TTL   int64
}

func GetIndexWithAdd(indexname string) (int64, error) {
	//table := configGlobal.GetDynamoDBtable()
	result := Index{}
	//err := table.Update("prime", configGlobal.GetDynamoDBprefix()+"_INDEX").Range("sort", indexname).Add("Num", 1).Value(&result)
	err := configGlobal.DynamoTableUpdate("INDEX").Range("sort", indexname).Add("Num", 1).Value(&result)
	if err != nil {
		return -1, errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to update to dynamodb")
	}
	return result.Num, nil
}

func GetIndexWithAddWithTTL(indexname string, timeoutSecond int64) (int64, error) {
	//table := configGlobal.GetDynamoDBtable()
	result := Index{}
	//err := table.Update("prime", configGlobal.GetDynamoDBprefix()+"_INDEX").Range("sort", indexname).Add("Num", 1).Value(&result)
	err := configGlobal.DynamoTableUpdate("INDEX").Range("sort", indexname).Add("Num", 1).Set("TTL", time.Now().Unix()+timeoutSecond).Value(&result)
	if err != nil {
		return -1, errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to update to dynamodb")
	}
	return result.Num, nil
}
