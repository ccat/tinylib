//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"testing"
)

func TestUser1Normal(t *testing.T) {
	configGlobal = nil
	_, err := GetConfig()
	if err != nil {
		t.Fatalf("failed GetConfig %#v", err)
	}
	u := user{}
	u.Sort = "anonymous"
	if u.IsAnonymous() == false {
		t.Fatalf("failed IsAnonymous %#v", u)
	}
	if u.GetSort() != "anonymous" {
		t.Fatalf("failed GetSort %#v", u)
	}
	u2 := GetAnonymousUser()
	if u2.GetSort() != "anonymous" {
		t.Fatalf("failed GetSort %#v", u2)
	}
	u2, err = CreateLocalUser("testuser", "password", "nickname")
	if err != nil {
		t.Fatalf("failed CreateLocalUser %#v", err)
	}
	if u2.GetSort() != "testuser@local" {
		t.Fatalf("failed CreateLocalUser %#v", u2)
	}
	if u2.IsAdmin() != false {
		t.Fatalf("failed CreateLocalUser: IsAdmin %#v", u2)
	}
	err = CreateLocalRootUser("testadmin", "testadminpassword")
	if err != nil {
		t.Fatalf("failed CreateLocalRootUser %#v", err)
	}
	u2, err = LoginLocalUser("testadmin", "testadminpassword")
	if err != nil {
		t.Fatalf("failed LoginLocalUser %#v", err)
	}
	if u2.GetSort() != "testadmin@local" {
		t.Fatalf("failed LoginLocalUser %#v", u2)
	}
	if u2.IsAdmin() != true {
		t.Fatalf("failed LoginLocalUser: IsAdmin %#v", u2)
	}
	u2, err = GetUser("testuser@local")
	if err != nil {
		t.Fatalf("failed GetUser %#v", err)
	}
	if u2.GetSort() != "testuser@local" {
		t.Fatalf("failed GetUser %#v", u2)
	}
	u2, err = GetOrNewUser("testuser3", "local")
	if err != nil {
		t.Fatalf("failed GetOrNewUser %#v", err)
	}
	if u2.GetSort() != "testuser3@local" {
		t.Fatalf("failed GetOrNewUser %#v", u2)
	}
	jsonStr, err := u2.GetJsonVal("jsonTest")
	if err != nil {
		t.Fatalf("failed GetJsonVal %#v", err)
	}
	if jsonStr != "" {
		t.Fatalf("failed GetJsonVal %#v", jsonStr)
	}
	err = u2.SetJsonVal("jsonTest", "json data")
	if err != nil {
		t.Fatalf("failed SetJsonVal %#v", err)
	}
	jsonStr, err = u2.GetJsonVal("jsonTest")
	if err != nil {
		t.Fatalf("failed GetJsonVal %#v", err)
	}
	if jsonStr != "json data" {
		t.Fatalf("failed GetJsonVal %#v", jsonStr)
	}
}

func TestUser2Error(t *testing.T) {
	configGlobal = nil
	_, err := GetConfig()
	if err != nil {
		t.Fatalf("failed GetConfig %#v", err)
	}
	u2, err := CreateLocalUser("testuser", "password2", "nickname2")
	if err != ErrorUserExists {
		t.Fatalf("failed CreateLocalUser: Double Created %#v,%#v", err, u2)
	}
	err = CreateLocalRootUser("testadmin", "testadminpassword2")
	if err != ErrorUserExists {
		t.Fatalf("failed CreateLocalRootUser: Double Created %#v,%#v", err, u2)
	}
	u2, err = LoginLocalUser("testadmin", "testadminpassword2")
	if err == nil {
		t.Fatalf("failed LoginLocalUser: Invalid Login %#v", u2)
	} else {
		t.Logf("LoginLocalUser: %#v", err)
	}
	u2, err = GetUser("testuser2@local")
	if err == nil {
		t.Fatalf("failed GetUser: Invalid userid %#v", u2)
	}
	if err != ErrorUserNotExists {
		t.Logf("failed GetUser: Invalid error %#v", err)
	}
}
