package tinylib

import (
	"fmt"
	"strings"
	//"net/http"
	//"time"
	"encoding/json"

	//"github.com/aws/aws-lambda-go/events"
	//jwt "github.com/dgrijalva/jwt-go"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type UserInterface interface {
	Save() error
	IsAnonymous() bool
	IsAdmin() bool
	GetSort() string
	SetNickname(string) error
	PasswordCheck(string) error
	GetJsonVal(string) (string, error)
	SetJsonVal(key string, val string) error
	SetBasicAttr(disabled bool, admin bool, staff bool) error
}

type user struct {
	Prime       string `dynamo:"prime"` //"USER"
	Sort        string `dynamo:"sort"`  //Email+provider
	Email       string
	Provider    string
	Hash        string
	Role        []string
	SecondIndex string
	IndexId     int64
	Nickname    string
	Admin       bool
	Staff       bool
	Disabled    bool
	Json        string
	JsonDic     map[string]string `dynamo:"-"`
}

var (
	ErrorUserExists    = errors.New("Specified User ID already exists.")
	ErrorUserNotExists = errors.New("Specified User ID does not existed.")
)

func createLocalUserInternal(userid string, nickname string, password string, admin bool) (*user, error) {
	userI := &user{}
	userI.Prime = configGlobal.GenerateDynamoDBPrime("USER")
	userI.Sort = userid + "@local"
	userI.Email = ""
	userI.Provider = "local"
	userI.Nickname = nickname
	if admin {
		userI.Admin = true
		userI.Staff = true
	} else {
		userI.Admin = false
		userI.Staff = false
	}
	userI.Disabled = false

	_, err := GetUser(userI.Sort)
	if err == nil {
		return nil, ErrorUserExists
	} else if err != ErrorUserNotExists {
		return nil, err
	}

	ind, err := GetIndexWithAdd("userid")
	if err != nil {
		return nil, err
	}
	userI.IndexId = ind
	userI.SecondIndex = fmt.Sprintf("%05d", ind)

	passwordBytes := []byte(password + configGlobal.GetPasswordSalt())
	cost := 10
	hashB, _ := bcrypt.GenerateFromPassword(passwordBytes, cost)
	userI.Hash = string(hashB)
	return userI, nil
}

func CreateLocalRootUser(userid string, password string) error {
	userI, err := createLocalUserInternal(userid, "admin", password, true)
	if err != nil {
		return err
	}
	err = userI.Save()
	return err
}

func CreateLocalUser(userid string, password string, nickname string) (UserInterface, error) {
	userI, err := createLocalUserInternal(userid, nickname, password, false)
	if err != nil {
		return nil, err
	}
	err = userI.Save()
	return userI, err
}

func GetOrNewUser(email string, provider string) (UserInterface, error) {
	u, err := GetUser(email + "@" + provider)
	if err == nil {
		return u, nil
	} else if err != ErrorUserNotExists {
		return nil, err
	}

	userI := &user{}
	userI.Prime = configGlobal.GenerateDynamoDBPrime("USER")
	userI.Sort = email + "@" + provider
	userI.Email = email
	userI.Provider = provider
	userI.Nickname = strings.Split(email, "@")[0]
	userI.Admin = false
	userI.Staff = false
	userI.Disabled = false

	ind, err := GetIndexWithAdd("userid")
	if err != nil {
		return nil, err
	}
	userI.IndexId = ind
	userI.SecondIndex = fmt.Sprintf("%05d", ind)

	err = userI.Save()
	return userI, err
}

func LoginLocalUser(userid string, password string) (UserInterface, error) {
	u, err := GetUser(userid + "@local")
	if err != nil {
		return nil, err
	}
	err = u.PasswordCheck(password)
	return u, err
}

func GetUser(sort string) (UserInterface, error) {
	//table := configGlobal.GetDynamoDBtable()

	result := []user{}
	//err := table.Get("prime", configGlobal.GetDynamoDBprefix()+"_USER").Range("sort", dynamo.Equal, sort).All(&result)
	err := configGlobal.DynamoTableGet("USER").Range("sort", dynamo.Equal, sort).All(&result)
	if err != nil {
		return nil, err
	}
	if len(result) == 0 {
		return nil, ErrorUserNotExists
	}
	return &result[0], nil
}

func GetAllUser(pageKey string) ([]UserInterface, string, error) {
	//table := configGlobal.GetDynamoDBtable()
	pKey := dynamo.PagingKey{}
	pFlag := false
	result := []UserInterface{}

	if pageKey != "" {
		err := json.Unmarshal([]byte(pageKey), &pKey)
		if err != nil {
			return result, pageKey, err
		}
		pFlag = true
	}

	tempResult := []user{}
	//q := table.Get("prime", configGlobal.GenerateDynamoDBPrime("USER"))
	q := configGlobal.DynamoTableGet("USER")
	if pFlag {
		q = q.StartFrom(pKey)
	}
	pKey, err := q.AllWithLastEvaluatedKey(&tempResult)
	if err != nil {
		return result, pageKey, err
	}
	for i := 0; i < len(tempResult); i++ {
		result = append(result, UserInterface(&tempResult[i]))
	}
	if pKey != nil {
		pageKeyByte := []byte{}
		pageKeyByte, err = json.Marshal(pKey)
		pageKey = string(pageKeyByte)
	} else {
		pageKey = ""
	}

	return result, pageKey, err
}

func GetAnonymousUser() UserInterface {
	userI := &user{}
	userI.Prime = configGlobal.GenerateDynamoDBPrime("USER")
	userI.Sort = "anonymous"
	userI.Email = ""
	userI.Provider = "local"
	userI.Nickname = "anonymous"
	userI.Admin = false
	userI.Staff = false
	userI.Disabled = false
	userI.IndexId = 0
	userI.SecondIndex = fmt.Sprintf("%05d", 0)
	return userI
}

func (self *user) Save() error {
	if self.Prime == "" {
		self.Prime = configGlobal.GenerateDynamoDBPrime("USER")
	}
	if self.IsAnonymous() {
		return nil
	}
	//table := configGlobal.GetDynamoDBtable()
	//return table.Put(self).Run()
	return configGlobal.DynamoTablePut(self).Run()
}

func (self *user) IsAnonymous() bool {
	return self.Sort == "anonymous"
}

func (self *user) IsAdmin() bool {
	return self.Admin
}

func (self *user) GetSort() string {
	return self.Sort
}

func (self *user) SetBasicAttr(disabled bool, admin bool, staff bool) error {
	self.Disabled = disabled
	self.Admin = admin
	self.Staff = staff
	return self.Save()
}

func (self *user) SetNickname(nickname string) error {
	self.Nickname = nickname
	return self.Save()
}

func (self *user) PasswordCheck(password string) error {
	passwordBytes := []byte(password + configGlobal.GetPasswordSalt())
	return bcrypt.CompareHashAndPassword([]byte(self.Hash), passwordBytes)
}

func (self *user) GetJsonVal(key string) (string, error) {
	if self.JsonDic == nil {
		err := self.parseJson()
		if err != nil {
			return "", err
		}
	}
	if val, ok := self.JsonDic[key]; ok {
		return val, nil
	}
	return "", nil
}

func (self *user) SetJsonVal(key string, val string) error {
	if self.JsonDic == nil {
		err := self.parseJson()
		if err != nil {
			return err
		}
	}

	self.JsonDic[key] = val
	jsonData, err := json.Marshal(self.JsonDic)
	if err != nil {
		return err
	}
	self.Json = string(jsonData)
	return self.Save()
}

func (self *user) parseJson() error {
	if self.Json == "" {
		self.JsonDic = map[string]string{}
		return nil
	}
	tempVal := map[string]string{}
	//var tempVal interface{}
	err := json.Unmarshal([]byte(self.Json), &tempVal)
	if err != nil {
		self.JsonDic = map[string]string{}
		return err
	}
	self.JsonDic = tempVal

	return nil
}
