//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func Test1sqs(t *testing.T) {
	configGlobal = nil
	err := os.Setenv("TEST_MODE", "TRUE")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("AWS_REGION", "us-east-1")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("DynamoDB_TABLE_PREFIX", "testPrefix")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("DynamoDB_TABLE_SUFFIX", "testSuffix")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	conf, err := GetConfig()
	if err != nil {
		t.Fatalf("failed GetConfig %#v", err)
	}

	tempSqs := sqs.New(session.New(), &aws.Config{Endpoint: aws.String("http://127.0.0.1:9324"), Region: aws.String("us-east-1")})
	//SqsBaseURL := "http://127.0.0.1:9324/queue/"
	params := &sqs.CreateQueueInput{
		QueueName: aws.String("testqueue"),
	}
	resp, err := tempSqs.CreateQueue(params)
	if err != nil {
		t.Logf("baseURL:%v", conf.GetSQSBaseURL())
		t.Fatalf("tinylib.CreateQueue():%v,%v", err, resp)
	}

	message := map[string]string{}
	message["test"] = "testM"
	message["test2"] = "testM2"
	err = SendSQSMessage(message, "testqueue", 0)
	if err != nil {
		t.Fatalf("failed SendSQSMessage %#v", err)
	}
	params2 := &sqs.ReceiveMessageInput{
		QueueUrl: aws.String(conf.GetSQSBaseURL() + "testqueue"), // Required
	}
	resp2, err := conf.GetSQS().ReceiveMessage(params2)
	if err != nil {
		t.Fatalf("failed ReceiveMessage %#v", err)
	}
	jsonStr := resp2.Messages[0].Body
	jsonMap := map[string]string{}
	err = json.Unmarshal([]byte(*jsonStr), &jsonMap)
	if err != nil {
		t.Fatalf("failed Unmarshal %#v,%#v", err, jsonStr)
	}
	if jsonMap["test"] != "testM" || jsonMap["test2"] != "testM2" {
		t.Fatalf("failed Unmarshal data %#v,%#v", jsonStr, jsonMap)
	}
}
