//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

func TestMain(m *testing.M) {
	m.Run()
	/*setup()
	  ret := m.Run()
	  if ret == 0 {
	      teardown()
	  }*/
}

func Test1GetConfig(t *testing.T) {
	configGlobal = nil
	err := os.Setenv("TEST_MODE", "TRUE")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("AWS_REGION", "us-east-1")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("DynamoDB_TABLE_PREFIX", "testPrefix")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	err = os.Setenv("DynamoDB_TABLE_SUFFIX", "testSuffix")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	conf, err := GetConfig()
	if err != nil {
		t.Fatalf("failed GetConfig %#v", err)
	}
	if conf.GetAWSregion() != "us-east-1" {
		t.Fatalf("failed AWS_REGION %#v", conf.GetAWSregion())
	}
	if conf.GenerateDynamoDBPrime("prime") != "testPrefix_prime_testSuffix" {
		t.Fatalf("failed GenerateDynamoDBPrime %#v", conf.GenerateDynamoDBPrime("prime"))
	}
	if len(conf.GetSessionSalt()) != 32 {
		t.Fatalf("failed GetSessionSalt %#v,%#v", len(conf.GetSessionSalt()), conf.GetSessionSalt())
	}
	conf2 := Config{}
	conf2.AWS_REGION = os.Getenv("AWS_REGION")
	conf2.DynamoDB_TABLE_NAME = os.Getenv("DynamoDB_TABLE")
	conf2.DynamoDB_TABLE_PREFIX = os.Getenv("DynamoDB_TABLE_PREFIX")
	conf2.DynamoDB_TABLE_SUFFIX = os.Getenv("DynamoDB_TABLE_SUFFIX")
	conf2.SESSION_SALT = os.Getenv("SESSION_SALT")
	conf2.PASSWORD_SALT = os.Getenv("PASSWORD_SALT")
	db := dynamo.New(session.New(), &aws.Config{
		Endpoint: aws.String("http://localhost:8000"),
	})
	conf2.DynamoTable = db.Table("testTable")
	err = conf2.getOrGenerateSALT()
	if err != nil {
		t.Fatalf("failed getOrGenerateSALT %#v", err)
	}
	sSalt1 := conf2.GetSessionSalt()
	err = conf2.getOrGenerateSALT()
	if err != nil {
		t.Fatalf("failed getOrGenerateSALT %#v", err)
	}
	sSalt2 := conf2.GetSessionSalt()
	if sSalt1 != sSalt2 {
		t.Fatalf("failed getOrGenerateSALT: Salt changed %#v != %#v", sSalt1, sSalt2)
	}
}

func Test0ConfigGetFuncs(t *testing.T) {
	configGlobal = nil
	err := os.Setenv("TEST_MODE", "TRUE")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	conf := Config{}
	conf.DynamoDB_TABLE_NAME = "aaa"
	if conf.GetDynamoDBtableName() != "aaa" {
		t.Fatalf("failed GetDynamoDBtableName %#v", conf.GetDynamoDBtableName())
	}
	conf.SESSION_SALT = "aaa"
	conf.SESSION_SALT_DB = "1234567891011121314151617181920123456789101112131415161718192011"
	if conf.GetSessionSalt() != "aaa12345678910111213141516171819" {
		t.Fatalf("failed GetSessionSalt %#v", conf.GetSessionSalt())
	}
	conf.PASSWORD_SALT = "aaa"
	conf.PASSWORD_SALT_DB = "3"
	if conf.GetPasswordSalt() != "aaa3" {
		t.Fatalf("failed GetPasswordSalt %#v", conf.GetPasswordSalt())
	}
	if conf.GetAESkey() != "aaa3                            " {
		t.Fatalf("failed GetAESkey %#v", conf.GetAESkey())
	}
	conf.PASSWORD_SALT = "aaa"
	conf.PASSWORD_SALT_DB = "abcdefghijklmnopqrstuvwxyz1234567890"
	if conf.GetAESkey() != "aaaabcdefghijklmnopqrstuvwxyz123" {
		t.Fatalf("failed GetAESkey %#v", conf.GetAESkey())
	}
}

func TestConfigEncryptions(t *testing.T) {
	err := os.Setenv("TEST_MODE", "TRUE")
	if err != nil {
		t.Fatalf("failed os.Setenv %#v", err)
	}
	conf := &Config{}
	conf.PASSWORD_SALT = "abcdef"
	conf.PASSWORD_SALT_DB = "abcdefghijklmnopqrstuvwxyz1234567890"
	configGlobal = conf
	byteLine, err := EncryptByAES("hello encrypt")
	if err != nil {
		t.Fatalf("failed EncryptByAES %#v", err)
	}
	decryptByteLine, err := DecryptByAES(byteLine)
	if err != nil {
		t.Fatalf("failed DecryptByAES %#v", err)
	}
	if string(decryptByteLine) != "hello encrypt" {
		t.Fatalf("failed DecryptByAES %#v", decryptByteLine)
	}

	strLine, err := EncryptByAESwithBase64("hello encrypt with base64")
	if err != nil {
		t.Fatalf("failed EncryptByAESwithBase64 %#v", err)
	}
	strLine, err = DecryptByAESwithBase64(strLine)
	if err != nil {
		t.Fatalf("failed DecryptByAESwithBase64 %#v", err)
	}
	if strLine != "hello encrypt with base64" {
		t.Fatalf("failed DecryptByAESwithBase64 %#v", strLine)
	}
}
