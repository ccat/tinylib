//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/pkg/errors"
)

func SendEmail(from string, to []string, title string, body string) error {
	//svc := ses.New(session.NewSession())
	svc := ses.New(session.New())
	tos := []*string{}
	for i := 0; i < len(to); i++ {
		tos = append(tos, aws.String(to[i]))
	}

	input := &ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: tos,
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Text: &ses.Content{
					Charset: aws.String("UTF-8"),
					Data:    aws.String(body),
				},
			},
			Subject: &ses.Content{
				Charset: aws.String("UTF-8"),
				Data:    aws.String(title),
			},
		},
		Source: aws.String(from),
	}
	_, err := svc.SendEmail(input)
	if err != nil {
		return errors.New(err.Error())
	}
	return nil
}
