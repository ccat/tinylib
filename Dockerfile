FROM golang:1.11.4

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install openjdk-8-jre awscli -y

RUN mkdir /tmp/dynamodb
RUN wget -O - https://s3-ap-southeast-1.amazonaws.com/dynamodb-local-singapore/dynamodb_local_latest.tar.gz | tar xz --directory /tmp/dynamodb
#RUN wget -O - http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_latest | tar xz --directory /tmp/dynamodb

RUN mkdir /tmp/elasticmq
RUN wget -O /tmp/elasticmq/elasticmq-server-0.14.7.jar https://s3-eu-west-1.amazonaws.com/softwaremill-public/elasticmq-server-0.14.7.jar

#  - java -Djava.library.path=/tmp/dynamodb/DynamoDBLocal_lib -jar /tmp/dynamodb/DynamoDBLocal.jar -sharedDb -inMemory &
#  - sleep 2
#WORKDIR /go/src/app
#COPY . .
#
#RUN go get -d -v ./...
#RUN go install -v ./...
