//Package tinylib provides small functions for aws api gateway with lambda
package tinylib

import (
	"crypto/aes"
	"crypto/cipher"
	crand "crypto/rand"
	"encoding/base64"
	"io"
	"log"
	"math/rand"
	"os"
	"time"

	//"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	//"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	//"github.com/ccat/copy"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/guregu/dynamo"
	"github.com/pkg/errors"
)

type ConfigInterface interface {
	GetAWSregion() string
	GetDynamoDBtableName() string
	//GetDynamoDBprefix() string
	GenerateDynamoDBPrime(prime string) string
	DynamoTableGet(prime string) *dynamo.Query
	DynamoTablePut(item interface{}) *dynamo.Put
	DynamoTableWriteTx() *dynamo.WriteTx
	DynamoTableDelete(prime string) *dynamo.Delete
	DynamoTableUpdate(prime string) *dynamo.Update
	GetSessionSalt() string
	GetPasswordSalt() string
	GetAESkey() string
	GetDynamoDBtable() dynamo.Table
	GetS3() *s3.S3
	GetSQS() *sqs.SQS
	GetSQSBaseURL() string
}

type Config struct {
	AWS_REGION            string
	SESSION_SALT          string
	SESSION_SALT_DB       string
	PASSWORD_SALT         string
	PASSWORD_SALT_DB      string
	DynamoDB_TABLE_NAME   string
	DynamoDB_TABLE_PREFIX string
	DynamoDB_TABLE_SUFFIX string
	DynamoDb              *dynamo.DB
	DynamoTable           dynamo.Table
	Sqs                   *sqs.SQS
	SqsBaseURL            string
}

type salt struct {
	Prime string `dynamo:"prime"` //"_SESSION_SALT"
	Sort  string `dynamo:"sort"`  //"SALT"
	Salt  string
}

var configGlobal ConfigInterface

func GetConfig() (ConfigInterface, error) {
	if configGlobal != nil {
		return configGlobal, nil
	}

	conf := &Config{}
	configGlobal = conf

	conf.AWS_REGION = os.Getenv("AWS_REGION")
	conf.DynamoDB_TABLE_NAME = os.Getenv("DynamoDB_TABLE")
	conf.DynamoDB_TABLE_PREFIX = os.Getenv("DynamoDB_TABLE_PREFIX")
	conf.DynamoDB_TABLE_SUFFIX = os.Getenv("DynamoDB_TABLE_SUFFIX")
	conf.SESSION_SALT = os.Getenv("SESSION_SALT")
	conf.PASSWORD_SALT = os.Getenv("PASSWORD_SALT")
	conf.SqsBaseURL = os.Getenv("SQS_BASE_URL")
	testMode := os.Getenv("TEST_MODE")
	if testMode == "" {
		db := dynamo.New(session.New(), &aws.Config{
			Region: aws.String(conf.AWS_REGION),
		})
		conf.DynamoDb = db
		conf.DynamoTable = db.Table(conf.DynamoDB_TABLE_NAME)

		err := conf.getOrGenerateSALT()
		if err != nil {
			log.Printf("tinylib.GetConfig():%v", err)
			return configGlobal, errors.Wrap(err, "tinylib.GetConfig(): failed to generate salt")
		}
		conf.Sqs = sqs.New(session.New())
	} else { //Only for test
		//creds := credentials.NewStaticCredentials("testkey", "testsecret", "")
		//creds := credentials.NewEnvCredentials()
		db := dynamo.New(session.New(), &aws.Config{
			Endpoint: aws.String("http://localhost:8000"),
			//Credentials: creds,
		})
		conf.DynamoDb = db
		conf.DynamoTable = db.Table("testTable")

		err := conf.getOrGenerateSALT()
		if err != nil {
			log.Printf("tinylib.GetConfig():%v", err)
			return configGlobal, errors.Wrap(err, "tinylib.GetConfig(): failed to generate salt")
		}
		conf.Sqs = sqs.New(session.New(), &aws.Config{Endpoint: aws.String("http://127.0.0.1:9324"), Region: aws.String("us-east-1")})
		conf.SqsBaseURL = "http://127.0.0.1:9324/queue/"
	}

	return configGlobal, nil
}

func (self *Config) GetAWSregion() string {
	return self.AWS_REGION
}

func (self *Config) GetDynamoDBtableName() string {
	return self.DynamoDB_TABLE_NAME
}

/*func (self *Config) GetDynamoDBprefix() string {
	return self.DynamoDB_TABLE_PREFIX
}*/

func (self *Config) GenerateDynamoDBPrime(prime string) string {
	tempStr := ""
	if self.DynamoDB_TABLE_PREFIX != "" {
		tempStr = self.DynamoDB_TABLE_PREFIX + "_"
	}
	tempStr = tempStr + prime
	if self.DynamoDB_TABLE_SUFFIX != "" {
		tempStr = tempStr + "_" + self.DynamoDB_TABLE_SUFFIX
	}
	return tempStr
}

func (self *Config) DynamoTableGet(prime string) *dynamo.Query {
	table := self.GetDynamoDBtable()
	return table.Get("prime", self.GenerateDynamoDBPrime(prime))
}

func (self *Config) DynamoTableUpdate(prime string) *dynamo.Update {
	table := self.GetDynamoDBtable()
	return table.Update("prime", self.GenerateDynamoDBPrime(prime))
}

func (self *Config) DynamoTablePut(item interface{}) *dynamo.Put {
	table := self.GetDynamoDBtable()
	return table.Put(item)
}

func (self *Config) DynamoTableWriteTx() *dynamo.WriteTx {
	return self.DynamoDb.WriteTx()
}

func (self *Config) DynamoTableDelete(prime string) *dynamo.Delete {
	table := self.GetDynamoDBtable()
	return table.Delete("prime", prime)
}

func (self *Config) GetSessionSalt() string {
	temp := self.SESSION_SALT + self.SESSION_SALT_DB
	return temp[:32]
}

func (self *Config) GetPasswordSalt() string {
	return self.PASSWORD_SALT + self.PASSWORD_SALT_DB
}

func (self *Config) GetAESkey() string {
	tempSalt := self.PASSWORD_SALT + self.PASSWORD_SALT_DB
	if len(tempSalt) > 32 {
		tempSalt = tempSalt[:32]
	}
	for len(tempSalt) < 32 {
		tempSalt = tempSalt + " "
	}
	return tempSalt
}

func (self *Config) GetDynamoDBtable() dynamo.Table {
	return self.DynamoTable
}

func (self *Config) GetS3() *s3.S3 {
	return s3.New(session.New(), &aws.Config{
		Region: aws.String(self.AWS_REGION),
	})
}

func (self *Config) GetSQS() *sqs.SQS {
	return self.Sqs
}

func (self *Config) GetSQSBaseURL() string {
	return self.SqsBaseURL
}

func (self *Config) getOrGenerateSALT() error {
	rand.Seed(time.Now().UTC().UnixNano())
	source_str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	strlen := 128

	//table := self.GetDynamoDBtable()
	result := []salt{}
	//err := table.Get("prime", self.GetDynamoDBprefix()+"_SESSION_SALT").All(&result)
	err := self.DynamoTableGet("SESSION_SALT").All(&result)
	if err != nil {
		return errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to GET")
	}

	if len(result) > 0 {
		self.SESSION_SALT_DB = result[0].Salt
	} else {
		session := &salt{}
		//session.Prime = self.GetDynamoDBprefix() + "_SESSION_SALT"
		session.Prime = self.GenerateDynamoDBPrime("SESSION_SALT")
		session.Sort = "SALT"

		tempR := make([]byte, strlen)
		for i := 0; i < strlen; i++ {
			tempR[i] = source_str[rand.Intn(len(source_str))]
		}
		session.Salt = string(tempR)
		//err = table.Put(session).Run()
		err = self.DynamoTablePut(session).Run()
		if err != nil {
			return errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to Put to dynamodb")
		}
		self.SESSION_SALT_DB = session.Salt
	}

	result = []salt{}
	//err = table.Get("prime", self.GetDynamoDBprefix()+"_PASSWORD_SALT").All(&result)
	err = self.DynamoTableGet("PASSWORD_SALT").All(&result)
	if err != nil {
		return errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to Get to dynamodb")
	}
	if len(result) > 0 {
		self.PASSWORD_SALT_DB = result[0].Salt
	} else {
		session := &salt{}
		session.Prime = self.GenerateDynamoDBPrime("PASSWORD_SALT")
		session.Sort = "SALT"

		tempR := make([]byte, strlen)
		for i := 0; i < strlen; i++ {
			tempR[i] = source_str[rand.Intn(len(source_str))]
		}
		session.Salt = string(tempR)
		err = self.DynamoTablePut(session).Run()
		if err != nil {
			return errors.Wrap(err, "tinylib.getOrGenerateSALT(): failed to Put to dynamodb")
		}
		self.PASSWORD_SALT_DB = session.Salt
	}
	return nil
}

func EncryptByAES(plainText string) ([]byte, error) {
	cipherText := make([]byte, aes.BlockSize+len(plainText))
	block, err := aes.NewCipher([]byte(configGlobal.GetAESkey()))
	if err != nil {
		return cipherText, errors.Wrap(err, "tinylib.EncryptByAES(): failed to encrypt")
	}

	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(crand.Reader, iv); err != nil {
		return cipherText, errors.Wrap(err, "tinylib.EncryptByAES(): failed to generate string")
	}
	encryptStream := cipher.NewCTR(block, iv)
	encryptStream.XORKeyStream(cipherText[aes.BlockSize:], []byte(plainText))
	return cipherText, nil
}

func EncryptByAESwithBase64(plainText string) (string, error) {
	byteResult, err := EncryptByAES(plainText)
	if err != nil {
		return "", errors.Wrap(err, "tinylib.EncryptByAESwithBase64(): failed to encrypt")
	}
	result := base64.StdEncoding.EncodeToString(byteResult)
	return result, nil
}

func DecryptByAES(cipherText []byte) ([]byte, error) {
	decryptedText := make([]byte, len(cipherText[aes.BlockSize:]))
	block, err := aes.NewCipher([]byte(configGlobal.GetAESkey()))
	if err != nil {
		return decryptedText, errors.Wrap(err, "tinylib.DecryptByAES(): failed to encrypt")
	}

	decryptStream := cipher.NewCTR(block, cipherText[:aes.BlockSize])
	decryptStream.XORKeyStream(decryptedText, cipherText[aes.BlockSize:])

	return decryptedText, nil
}

func DecryptByAESwithBase64(cipherText string) (string, error) {
	byteCipherText, err := base64.StdEncoding.DecodeString(cipherText)
	if err != nil {
		return "", errors.Wrap(err, "tinylib.DecryptByAESwithBase64(): failed to decrypt")
	}
	byteResult, err := DecryptByAES(byteCipherText)
	return string(byteResult), err
}
